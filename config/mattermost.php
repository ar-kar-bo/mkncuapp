<?php

return [
    'url' => env('MATTERMOST_URL', 'http://localhost'),
    'userUrl' => env('MATTERMOST_USERURL', 'http://localhost'),
    'teamUrl' => env('MATTERMOST_TEAMURL', 'http://localhost'),
    'activate' => env('MATTERMOST_ACTIVATE', 'http://localhost'),
    'deactivate' => env('MATTERMOST_DEACTIVATE', 'http://localhost'),
    'teamId' => env('MATTERMOST_TEAMID', 'http://localhost'),
    'token' => env('MATTERMOST_TOKEN', 'TOKENID'),
];
