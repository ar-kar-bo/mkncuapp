<?php

return [
    // Database Table; user.verification
    'Verification' => [
        'OK' => '001',
        'NG' => '800',
        'NOT_YET' => '999',
    ],

    // Database Table; user.flag
    'Flag' => [
        'ALIVE' => '0',
        'DELETED' => '9',
    ],
];
