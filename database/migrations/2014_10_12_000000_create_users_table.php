<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->string('password');
            $table->string('email')->unique();
            $table->string('phone_no')->nullable();
            $table->string('login_name');
            $table->string('intake_year');
            $table->string('department')->nullable();
            $table->string('agent_passcode');
            $table->string('ip_address')->nullable();
            $table->string('status')->default('alumni');
            $table->string('role')->default('user');
            $table->string('verification')->default('NotYet');
            $table->timestamp('last_login')->nullable();
            $table->string('note')->nullable();
            $table->integer('flag')->default(0);
            $table->string('community_status')->default('x');
            $table->integer('updated_by')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
