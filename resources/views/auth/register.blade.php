@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('စာရင်းသွင်းရန် (ကွက်လပ်များအားလုံး English လိုဖြည့်ပေးပါ)') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="agent_passcode" class="col-md-4 col-form-label text-md-right">{{ __('ကိုယ်စားလှယ်ထံမှ Passcode') }}</label>

                            <div class="col-md-6">
                                <input id="agent_passcode" type="text" class="form-control @error('agent_passcode') is-invalid @enderror" name="agent_passcode" value="{{ old('agent_passcode') }}" required autocomplete="agent_passcode"  autofocus>

                                @error('agent_passcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('အမည်') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name">

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Member Password (New)') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Member Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_no" class="col-md-4 col-form-label text-md-right">{{ __('ဖုန်းနံပါတ်') }}</label>

                            <div class="col-md-6">
                                <input id="phone_no" type="text" class="form-control @error('phone_no') is-invalid @enderror" name="phone_no" value="{{ old('phone_no') }}" autocomplete="phone_no" >

                                @error('phone_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="intake_year" class="col-md-4 col-form-label text-md-right">{{ __('ကျောင်း၀င်နှစ်') }}</label>

                            <div class="col-md-6">
                                <input id="intake_year" type="text" class="form-control @error('intake_year') is-invalid @enderror" name="intake_year" value="{{ old('intake_year') }}" required autocomplete="intake_year" placeholder="eg; 2000">

                                @error('intake_year')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('လက်ရှိကျောင်းနှင့်ပတ်သက်မှု') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" id="status" name="status" value="{{ old('status') }}">
                                        <option value="current_student" @if (old('status')=='current_student')
                                            selected
                                        @endif>{{ __('CurrentStudent (လက်ရှိကျောင်းသား)') }}</option>
                                        <option value="teacher" @if (old('status')=='teacher')
                                        selected
                                        @endif>{{ __('Teacher (ဆရာ/ဆရာမ)') }}</option>
                                        <option value="alumni" @if (old('status')=='alumni')
                                        selected
                                        @endif>{{ __('Alumnai (ကျောင်းသားဟောင်း)') }}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="department" class="col-md-4 col-form-label text-md-right">{{ __('ဆရာ/ဆရာမ ဌာန') }}</label>

                            <div class="col-md-6">
                                <input id="department" type="text" class="form-control @error('department') is-invalid @enderror" name="department" value="{{ old('department') }}" autocomplete="department"  placeholder="ကျောင်းသားများဖြည့်ရန်မလိုပါ">

                                @error('department')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
