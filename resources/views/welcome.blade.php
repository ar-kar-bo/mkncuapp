@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="align-content: center">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <p>မြစ်ကြီးနား CU မောင်နှမများ Batch(1) ကစ လက်ရှိ တက်နေဆဲ ‌ကျောင်းသားများ အားလုံး စုစည်းရာ နေရာဖြစ်ပါတယ်။</p>
            <p>Community ကိုတော့ <a href="{{ config('mattermost.url') }}" target="_blank"><b> ဒီကနေ </b></a>  ၀င်လို့ရပါတယ်။ </p>

            <p>ကိုယ်စားလှယ် ဆီက Invitation Code နဲ့ <a href="{{ route('register') }}"><b>{{ __(' ဒီကနေ Register') }}</b></a> လုပ်နိုင်ပါတယ်။</p>
            @guest
                Waiting for you.
            @else
                <a href="{{ route('home') }}">{{ __('My Dashboard') }} ကို ပြန်သွားရန်။</a>
            @endguest
        </div>
    </div>
</div>
@endsection
