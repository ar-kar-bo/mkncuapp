<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'App\Http\Controllers\Auth\AuthController@register')->name('register');
Route::post('/register', 'App\Http\Controllers\Auth\AuthController@storeUser');

Route::get('/login', 'App\Http\Controllers\Auth\AuthController@login')->name('login');
Route::post('/login', 'App\Http\Controllers\Auth\AuthController@authenticate');
Route::get('logout', 'App\Http\Controllers\Auth\AuthController@logout')->name('logout');

Route::get('/home', 'App\Http\Controllers\Auth\UserController@home')->name('home');

Route::get('/edit/{userId?}', 'App\Http\Controllers\Auth\UserController@get')->name('edit');
Route::post('/edit', 'App\Http\Controllers\Auth\UserController@update');
