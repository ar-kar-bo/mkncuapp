**To develop; ကြိုတင်ပြင်ဆင်ထားရန်** 
1. PHP Version 7.3 အထက်ဖြစ်ရပါမယ်
1. Composer Install လုပ်ထားဖို့ လိုပါမယ်
1. git command ရအောင် လုပ်ထားဖို့ လိုပါမယ်
1. mysql/mysqlworkbench သွင်းထားပါ
1. mattermost သွင်းထားပါ 

အောက်က Command ရိုက်ကြည့်ပါ
```
php -v
composer --version
git --version
```

git clone လုပ်ပါ 
```
git clone https://gitlab.com/mkncu/mkncuapp.git
```

git clone လုပ်ပြီးရင် mkncuapp folder ၀င်ပါ။ အောက်က Command ရိုက်ပါ
```
copy .env.example .env 
```

.env file ကိုပြင်ပါ။ မိမိ local မှ env value ပြောင်းပါ။
```
DB_DATABASE=db_name
DB_USERNAME=db_user
DB_PASSWORD=db_user_password
..
MATTERMOST_URL=http://localhost
MATTERMOST_USERURL=http://localhost/api/v4/users?iid=
MATTERMOST_TEAMURL=http://localhost/api/v4/teams/iausf1dhw7nkfgyoy73z4mez5a/members
MATTERMOST_DEACTIVATE=http://localhost/api/v4/users/
MATTERMOST_ACTIVATE=http://localhost/api/v4/users/user_id/active
MATTERMOST_TEAMID=iausf1dhw7nkfgyoy73z4mez5a
MATTERMOST_TOKEN=wqmpoz6dw7ratkpgbppf1xgpfc
...
 ↓↓
(1) MATTERMOST_URL/localhost; ကိုယ့် local က mattermost link ထည့်ပါ 
    (http://localhost သွားကြည့်လို့ local က mattermost တက်လာရင် ပြောင်းစရာမလိုမလို)
(2) MATTERMOST_TOKEN ယူရန်; Mattermost ကနေ Account Setting ⇒ Security ⇒ Create personal access tokens.
(3) MATTERMOST_TEAMID ယူရန်; Command line ကနေ အောက်က command ကို run ပါ
  curl -i -H 'Authorization: Bearer wqmpoz6dw7ratkpgbppf1xgpfc' http://localhost/api/v4/teams/name/mkncu
                                   (wqmpoz6dw7ratkpgbppf1xgpfc: ကိုယ့် local က MATTERMOST_TOKEN value ကို ပြောင်းပါ)
```

.env file ကိုပြင် ပြီးလျှင် အောက်က Command ရိုက်ပါ
```
composer install
php artisan key:generate
php artisan migrate
```

Project ကို Run ရန်
```
php artisan serve
```
http://localhost:8000
<br/>

