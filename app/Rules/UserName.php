<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserName implements Rule
{
    private $intakeYear = null;
    private $status = null;

    public function __construct(string $intakeYear, string $status){
        $this->intakeYear = $intakeYear;
        $this->status = $status;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $loginName = strtolower(str_replace(' ', '', $value));

        if($this->status == 'teacher') {
            $loginName = $loginName . "_tchr";
        } else {
            $loginName = $loginName .  "_" . $this->intakeYear;
        }
        $agentCount = User::where('login_name', $loginName)->get()->count();

        if($agentCount > 0) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ကျောင်း၀င်နှစ် '. $this->intakeYear.' နှစ်အတွက် Registered လုပ်ပြီး အမည် ဖြစ်နေပါတယ်။ တခြားနာမည်သုံးပါ။ သို့မဟုတ် နာမည်မှာ 1,2 စသည်ဖြင့်ပေါင်းထည့်ပါ။';
    }
}
